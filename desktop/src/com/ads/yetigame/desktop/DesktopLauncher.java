package com.ads.yetigame.desktop;

import com.ads.yetigame.SnakeGame;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		System.setProperty("user.name","EnglishWords"); //на случай кириллицы в путях к библиотекам
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Snake";
		config.width = 770; //размеры окна - ширина
		config.height = 720; //размеры окна - высота
		new LwjglApplication(SnakeGame.getInstance(), config); //запуск класса из core (ядро) - нашего синглтона.
	}
}
