package com.ads.yetigame;


import com.ads.yetigame.Model.KeyBoardController;
import com.ads.yetigame.Model.Snake;
import com.ads.yetigame.Model.SnakePart;
import com.ads.yetigame.Model.World;
import com.ads.yetigame.Screens.*;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ga_nesterchuk on 16.01.2016.
 */
public class SnakeGame extends Game {
    public static final float WORLD_WIDTH = 770f; //ширина мира
    public static final float WORLD_HEIGHT = 720f; //высота мира
    public static final int TILE_SIZE = 70; //размер клетки на текстурном атласе
    public static final int OUTPUT_TILE_SIZE = 20; //размер клетки для вывода (будет изменено)
    public static final boolean DRAWRECTS = false; //флаг отмечающий - надо ли выводить описывающие ректанглы
    private static SnakeGame ourInstance = new SnakeGame(); //экземпляр игры - вызывает конструктор
    World world;

    //private static ShapeRenderer shapeRenderer;


    public static SnakeGame getInstance() {
        return ourInstance;
    }  //геттер экземпляра - позволяет получить экземпляр игры извне

    public SnakeGame() {
    } //приватный конструктор - обеспечивает единственность экземпляра

    private float ppuX; //коэффициент подобия по иксу
    private float ppuY; //коэффициент подобия по игреку
    private HashMap<String, TextureRegion> textureRegions; //хранилище кусков текстур

    private SpriteBatch batch;
    private ShapeRenderer shape;
    //private BitmapFont font;
    private GameMenuScreen gameMenuScreen;
    private SettingsScreen settingsScreen;
    private GameScreen gameScreen;
    private HelpScreen helpScreen;
    private ScoreScreen scoreScreen;
    private PauseScreen pauseScreen;


    public ShapeRenderer getShapeRenderer() {
        return shape;
    }

    @Override
    public void create() {
        ppuX = Gdx.graphics.getWidth() / WORLD_WIDTH;
        ppuY = Gdx.graphics.getHeight() / WORLD_HEIGHT;
        loadGraphics(); //загрузить графику
        batch = new SpriteBatch(); //позволяет отрисовывать текстуры
        shape = new ShapeRenderer(); //позволяет рисовать графические примитивы
        //font = new BitmapFont(); //позволяет рисовать текст разными шрифтами
        gameMenuScreen = new GameMenuScreen(batch,  textureRegions);
        settingsScreen = new SettingsScreen(batch, textureRegions);
        helpScreen = new HelpScreen(batch,  textureRegions);
        scoreScreen = new ScoreScreen(batch,  textureRegions);
        gameScreen = new GameScreen(batch,  textureRegions);
        pauseScreen = new PauseScreen(batch,textureRegions);
        moveToMainMenu();

    }


    private void loadGraphics() {
        textureRegions = new HashMap<String, TextureRegion>();
        textureRegions.put("snakeTail", new TextureRegion(new Texture(Utils.resolvePath("snakePart.png"))));
        textureRegions.put("snakeHead", new TextureRegion(new Texture(Utils.resolvePath("snakeHead.png"))));
        textureRegions.put("apple", new TextureRegion(new Texture(Utils.resolvePath("apple.png"))));

    }


    //textureRegions.get("solidbrick brown");


    public void moveToMainMenu() {
        setScreen(gameMenuScreen);
    }

    public void moveToSettings() {
        setScreen(settingsScreen);
    }

    public void moveToGame() {
        setScreen(gameScreen);
    }

    public void moveToHelp() {setScreen(helpScreen);
    }

    public void moveToScore() {setScreen(scoreScreen);
    }

    public void moveToPauseMenu(){setScreen(pauseScreen);
    }




    //геттеры и сеттеры коэффициентов подобия
    public float getPpuX() {
        return ppuX;
    }

    public void setPpuX(float ppuX) {
        this.ppuX = ppuX;
    }

    public float getPpuY() {
        return ppuY;
    }

    public void setPpuY(float ppuY) {
        this.ppuY = ppuY;
    }

    public HashMap<String, TextureRegion> getTextureRegions() {
        return textureRegions;
    }

    public void resetScreen() {
        if (Snake.checkBitten() == true) {
            gameScreen = new GameScreen(batch, textureRegions);

        }
    }
    }



