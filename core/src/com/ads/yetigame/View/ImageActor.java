package com.ads.yetigame.View;

import com.ads.yetigame.SnakeGame;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by ga_nesterchuk on 25.01.2016.
 */
public class ImageActor extends Actor {
    TextureRegion img;
    ShapeRenderer shapeRenderer;

    public ImageActor(TextureRegion img, float x, float y, float width, float height) {
        this.img = img;
        setPosition(x * SnakeGame.getInstance().getPpuX(), y * SnakeGame.getInstance().getPpuY());
        setSize(width * SnakeGame.getInstance().getPpuX(), height * SnakeGame.getInstance().getPpuY());
        shapeRenderer = SnakeGame.getInstance().getShapeRenderer();
    }

    public ImageActor(Texture img, float x, float y) {
        this(new TextureRegion(img), x, y, img.getWidth(), img.getHeight());
    }

    public ImageActor(Texture img, float x, float y, float width, float height) {
        this(new TextureRegion(img), x, y, width, height);
    }

    public ImageActor(TextureRegion img, float x, float y) {
        this(img, x, y, img.getRegionWidth(), img.getRegionHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(img, getX(), getY(), getWidth(), getHeight());
        if(SnakeGame.DRAWRECTS) {
            batch.end();
            if (shapeRenderer.getProjectionMatrix() == null) {
                shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
            }
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.rect(getX() - 1, getY() - 1, getWidth() + 2, getHeight() + 2);
            shapeRenderer.end();
            batch.begin();
        }
    }
}
