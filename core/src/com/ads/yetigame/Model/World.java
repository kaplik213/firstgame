package com.ads.yetigame.Model;

import com.ads.yetigame.View.ImageActor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import java.util.HashMap;
import java.util.Random;


/**
 * Created by Nikita on 27.02.2016.
 */
public class World extends Stage {
    Snake snake;
    Apple apple;



    public World(ScreenViewport screenViewport, SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        super(screenViewport, batch);
        apple = new Apple();
        snake = new Snake();
        addActor(snake);
        addActor(apple);
    }


    public Snake getSnake() {
        return snake;
    }

    public void act(float delta) {
        super.act(delta);
        snake.currentTime += delta;
        addActor(snake);
        addActor(apple);

        if (snake.currentTime >= snake.speed) {
            snake.currentTime = 0;
            snake.advance();
            snake.checkBitten();
            apple.eat();
            apple.addingPart();
            apple.nextApple();
        }
    }

}