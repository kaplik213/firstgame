package com.ads.yetigame.Model;

import com.ads.yetigame.SnakeGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.graphics.g2d.Batch;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Nikita on 17.05.2016.
 */
public class Apple extends Actor {
    public static TextureRegion appleImg = SnakeGame.getInstance().getTextureRegions().get("apple");
    public static float appleSize = Math.min(appleImg.getRegionHeight() * SnakeGame.getInstance().getPpuX(), appleImg.getRegionWidth() * SnakeGame.getInstance().getPpuY());

    public List<ApplePosition> apples;

    public Apple() {
        apples = new ArrayList<ApplePosition>();
        apples.add(new ApplePosition(MathUtils.random(21), MathUtils.random(19)));
    }
    public boolean eat() {
        SnakePart head = Snake.parts.get(0);
        int appleSize = apples.size();
        for(int i = 0; i<appleSize; i++) {
            ApplePosition position = apples.get(i);
            if (position.x == head.x && position.y == head.y) {
                return true;
            }
        }
        return false;
    }
    public void addingPart(){
        if(eat()==true){
            SnakePart end = Snake.parts.get(Snake.parts.size() - 1);
            Snake.parts.add(new SnakePart(end.x, end.y));
        }
    }

    public void nextApple() {
        if (eat() == true) {
            apples.get(apples.size()-1).remove();
            apples.remove(apples.size()-1);
            apples.add(new ApplePosition(MathUtils.random(21), MathUtils.random(19)));
        }
    }
    public void removeApple(){
        if(eat()==true){
            int appleSize = apples.size();
            ApplePosition position = apples.get(appleSize);
        }
    }
    public void draw(Batch batch, float parentAlpha) {
        for (ApplePosition apple : apples) {
            batch.draw(appleImg, apple.x * appleSize, apple.y * appleSize, appleSize, appleSize);
        }
    }
}





