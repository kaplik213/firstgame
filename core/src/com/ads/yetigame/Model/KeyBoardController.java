package com.ads.yetigame.Model;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;

import java.awt.KeyEventPostProcessor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Key;

import sun.security.jca.GetInstance;

import static com.badlogic.gdx.Input.*;
import static com.badlogic.gdx.Input.Keys.*;

/**
 * Created by 13k1103 on 07.05.2016.
 */
public class KeyBoardController implements InputProcessor {
    Snake snake;
    World world;

    public KeyBoardController(Snake snake) {
        this.snake = snake;
    }

    @Override
    public boolean keyDown(int keycode) {
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case LEFT:
                if (snake.direction == snake.UP) {
                    snake.turnLeft();
                }
                if (snake.direction == snake.DOWN) {
                    snake.turnRight();
                }
                break;
            case RIGHT:
                if (snake.direction == snake.UP) {
                    snake.turnRight();
                }
                if (snake.direction == snake.DOWN) {
                    snake.turnLeft();
                }
                break;
            case UP:
                if (snake.direction == snake.LEFT) {
                    snake.turnRight();
                }
                if (snake.direction == snake.RIGHT) {
                    snake.turnLeft();
                }

                break;
            case DOWN:
                if (snake.direction == snake.LEFT) {
                    snake.turnLeft();
                }
                if (snake.direction == snake.RIGHT) {
                    snake.turnRight();
                }
                break;
            case A:
                snake.boost();

        }
        return false;
    }





    @Override
    public boolean keyTyped(char character) {
            return false;

    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }


    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }


}
