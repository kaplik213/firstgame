package com.ads.yetigame.Model;

import com.ads.yetigame.SnakeGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


/**
 * Created by 13k1103 on 16.04.2016.
 */

public class Snake extends Actor {
    public static TextureRegion headImg = SnakeGame.getInstance().getTextureRegions().get("snakeHead");
    public static TextureRegion tileImg = SnakeGame.getInstance().getTextureRegions().get("snakeTail");
    public static float headSize = Math.min(headImg.getRegionHeight() * SnakeGame.getInstance().getPpuX(), headImg.getRegionWidth() * SnakeGame.getInstance().getPpuY());
    public static float tileSize = Math.min(tileImg.getRegionHeight() * SnakeGame.getInstance().getPpuX(), tileImg.getRegionWidth() * SnakeGame.getInstance().getPpuY());
    public static int maxX = (int) (Gdx.graphics.getWidth() / tileSize);
    public static int maxY = (int) (Gdx.graphics.getHeight() / tileSize);

    public static final int UP = 0;
    public static final int LEFT = 1;
    public static final int DOWN = 2;
    public static final int RIGHT = 3;

    public float speed;
    public float currentTime;
    public static List<SnakePart> parts;//массив, который изменяется (тело змейки)

    public int direction;// направление


    public Snake() {// Создаем змейку
        direction = DOWN;// ? начальная позиция
        parts = new ArrayList<SnakePart>();
        parts.add(new SnakePart(5, 7));
        parts.add(new SnakePart(5, 8));
        parts.add(new SnakePart(5, 9));
        parts.add(new SnakePart(5, 10));
        currentTime = 0;
        speed = 0.1f;
    }


    public void turnLeft() {
        direction += 1;
        if (direction > RIGHT)
            direction = UP;
    }

    public void turnRight() {
        direction -= 1;
        if (direction < UP)
            direction = RIGHT;
    }





    public void advance() {
        SnakePart head = parts.get(0);
        int len = parts.size() - 1;
        for (int i = len; i > 0; i--) {
            SnakePart part = parts.get(i);
            SnakePart before = parts.get(i - 1);
            part.x = before.x;
            part.y = before.y;
        }
        switch (direction) {
            case UP:
                head.y += 1;
                break;
            case LEFT:
                head.x -= 1;
                break;
            case DOWN:
                head.y -= 1;
                break;
            case RIGHT:
                head.x += 1;
                break;

        }

        if (head.x < 0)
            head.x = maxX;
        else if (head.x > maxX)
            head.x = 0;
        else if (head.y < 0)
            head.y = maxY;
        else if (head.y > maxY)
            head.y = 0;
    }


    public static boolean checkBitten() {
        SnakePart head = parts.get(0);
        for (int i = 1; i < parts.size(); ++i) {
            SnakePart part = parts.get(i);
            if (part.x == head.x && part.y == head.y)
                return true;
        }
        return false;
    }
    public void draw(Batch batch,float parentAlpha) {
        SnakePart head = parts.get(0);
        for (SnakePart part : parts) {
            batch.draw(tileImg, part.x * tileSize, part.y * tileSize, tileSize, tileSize);
            batch.draw(headImg,head.x * headSize,head.y  * headSize,headSize,headSize);
        }
    }
    public void boost() {
        if (speed == 0.5f) {
            speed = 0.1f;
        }
        else{speed = 0.5f;}
    }

}


