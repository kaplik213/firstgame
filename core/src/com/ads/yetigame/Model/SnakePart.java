package com.ads.yetigame.Model;

import com.ads.yetigame.View.ImageActor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;

import java.util.Random;

/**
 * Created by 13k1103 on 16.04.2016.
 */
public class SnakePart extends Actor {

    public int x, y;

    public  SnakePart(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
