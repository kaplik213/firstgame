package com.ads.yetigame.Model;

import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;

import java.util.Map;

/**
 * Created by 13k1103 on 30.04.2016.
 */
public class SnakeController implements GestureDetector.GestureListener {
    Snake snake;
    float dX;
    float dY;
    public SnakeController(Snake snake) {
        this.snake = snake;
        dX = 0;
        dY = 0;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        dX +=deltaX;
        dY +=deltaY;
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        if(dX>0){
            snake.turnLeft();
        }else {
            snake.turnRight();
        }

        dX = 0;
        dY = 0;
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }
}
