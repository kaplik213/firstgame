package com.ads.yetigame.Screens;

import com.ads.yetigame.View.ImageActor;
import com.ads.yetigame.SnakeGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by Nikita on 27.02.2016.
 */
public class ScoreScreen implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor backButton;


    public ScoreScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        backGround = new ImageActor(new Texture("android/assets/main_menu_background.png"), 0, 0);

        backButton = new ImageActor(new Texture("android/assets/back-btn.png"), 36, 100);
        backButton.addListener(new  ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                SnakeGame.getInstance().moveToMainMenu();
            }
        });

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(backButton);


    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }
    @Override
    public void dispose() {
        stage.dispose();
    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}