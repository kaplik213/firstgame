package com.ads.yetigame.Screens;

import com.ads.yetigame.Model.Snake;
import com.ads.yetigame.View.ImageActor;
import com.ads.yetigame.SnakeGame;
import com.badlogic.gdx.Gdx;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.HashMap;

/**
 * Created by ga_nesterchuk on 16.01.2016.
 */
public class GameMenuScreen extends Snake implements Screen {
    private Stage stage;
    private ImageActor backGround;
    private ImageActor playButton;
    private ImageActor settingsButton;
    private ImageActor scoreButton;
    private ImageActor helpButton;


    public GameMenuScreen(SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {
        backGround = new ImageActor(new Texture("android/assets/main_menu_background.png"), 0, 0);

        playButton = new ImageActor(new Texture("android/assets/play-btn.png"), 307, 515);
        playButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                SnakeGame.getInstance().moveToGame();
            }
        });
        settingsButton = new ImageActor(new Texture("android/assets/settings-btn.png"), 36, 99);
        settingsButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                SnakeGame.getInstance().moveToSettings();
            }
        });
        scoreButton = new ImageActor(new Texture("android/assets/score-btn.png"), 290, 99);
        scoreButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                SnakeGame.getInstance().moveToScore();
            }
        });
        helpButton = new ImageActor(new Texture("android/assets/help-btn.png"),540,99);
        helpButton.addListener(new ClickListener() {
            public void clicked(InputEvent event, float x, float y) {
                SnakeGame.getInstance().moveToHelp();
            }
        });

        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        stage = new Stage(new ScreenViewport(camera), batch);

        stage.addActor(backGround);
        stage.addActor(playButton);
        stage.addActor(settingsButton);
        stage.addActor(scoreButton);
        stage.addActor(helpButton);


    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

}