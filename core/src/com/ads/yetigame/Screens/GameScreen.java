package com.ads.yetigame.Screens;

import com.ads.yetigame.Model.Apple;
import com.ads.yetigame.Model.KeyBoardController;

import com.ads.yetigame.Model.Snake;
import com.ads.yetigame.Model.SnakeController;
import com.ads.yetigame.Model.World;
import com.ads.yetigame.View.ImageActor;
import com.ads.yetigame.SnakeGame;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

/**
 * Created by Nikita on 27.02.2016.
 */
public class GameScreen  implements Screen {
    private World world;
    private ImageActor backGround;
    private ImageActor pause;
    private Stage userInterface;
    private SnakeController snakeController;
    private InputMultiplexer multiplexer;
    Apple apple;


    public GameScreen(final SpriteBatch batch, HashMap<String, TextureRegion> textureRegions) {


        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        world = new World(new ScreenViewport(camera), batch, textureRegions);
        userInterface = new Stage(new ScreenViewport(camera), batch);
        pause = new ImageActor(new Texture("android/assets/background.png"), 0, 0);
        pause.addListener(new ClickListener() {public void clicked(InputEvent event, float x, float y) {
            new SnakeGame().getInstance().moveToMainMenu();

            }
        });
        backGround = new ImageActor(new Texture("android/assets/background.png"), 0, 0);
        userInterface.addActor(backGround);
        userInterface.addActor(pause);
        userInterface.addActor(backGround);

        snakeController = new SnakeController(world.getSnake());
        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(userInterface);
        multiplexer.addProcessor(new GestureDetector(snakeController));
        multiplexer.addProcessor(new KeyBoardController(world.getSnake()));


    }



    @Override
    public void render(float delta) {
        userInterface.draw();
        world.draw();
        world.act(delta);
        userInterface.addActor(pause);
        SnakeGame.getInstance().resetScreen();


    }
    @Override
    public void show() {
        Gdx.input.setInputProcessor(multiplexer);


    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {


    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void  dispose() {

    }

}
