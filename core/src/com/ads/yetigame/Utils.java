package com.ads.yetigame;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;

import static com.badlogic.gdx.Application.ApplicationType.Android;
import static com.badlogic.gdx.Application.ApplicationType.Desktop;

/**
 * Created by 13k1103 on 16.04.2016.
 */
public class Utils {
    public static String resolvePath(String str) {
        String tmp = "android/assets/";
        return (
                Gdx.app.getType() == Desktop ?
                        String.format("%s%s", tmp, str) :
                        str
        );
    }
}
